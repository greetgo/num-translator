package kz.greetgo.num_translator.translator;

import kz.greetgo.num_translator.languages.Language;
import kz.greetgo.num_translator.mappers.ThreeDigitsBlock;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class NumTranslator {

  Language language;
  private boolean isDouble;

  public NumTranslator(Language lang) {
    this.language = lang;
  }

  /**
   * Translate(convert) number to words
   */
  public String translate(BigDecimal number) {
    if (number.compareTo(new BigDecimal(number.longValue())) == 0) {
      return new NumTranslator(language).translateFromLong(number.longValue());
    } else {
      return new NumTranslator(language).translateFromDouble(number.doubleValue());
    }
  }

  /**
   * Translate(convert) long to words
   */
  private String translateFromLong(long number) {

    if (number == 0) {
      return new ThreeDigitsBlock(0, this.language, 0, false).toWords();
    }

    String[] numberBlocks = splitByThreeDigits(number);

    List<String> result = new ArrayList<>();

    for (int i = 0; i < numberBlocks.length; i++) {
      int threeDigits = Integer.parseInt(numberBlocks[i]);

      if (threeDigits > 0) {
        ThreeDigitsBlock block = new ThreeDigitsBlock(threeDigits, language, numberBlocks.length - i - 1, isDouble);
        result.add(block.toWords());
      }
    }
    return String.join(" ", result);

  }

  /**
   * Translate(convert) double to words
   */
  private String translateFromDouble(Double number) {
    isDouble = true;
    boolean isZero = false;

    if (number == 0) {
      return new ThreeDigitsBlock(0, this.language, 0, isDouble).toWords();
    }

    number = round(number, 3); // round double number(after dot max number length is 3)

    var wholeAndDecimalParts = splitDoubleToWholeAndDecimal(number);

    List<String> result = new ArrayList<>();

    if (wholeAndDecimalParts[wholeAndDecimalParts.length - 1].equals("0")) {
      result.add(translateFromLong(number.longValue()));
      isZero = true;
    }

    if (!isZero) {
      String part = "";

      {
        part = wholeAndDecimalParts[0];

        if (!(this.language.equals(Language.ENG) && part.equals("0"))) {
          result.add(translateFromLong(Long.parseLong(part)));
          if (part.length() >= 3) {
            result.add(new ThreeDigitsBlock(Integer.parseInt(part.substring(part.length() - 3)), this.language, 0, isDouble).getFloatingPointInWord());
          } else {
            result.add(new ThreeDigitsBlock(Integer.parseInt(part), this.language, 0, isDouble).getFloatingPointInWord());
          }
        }
      }

      {
        part = wholeAndDecimalParts[1];

        if (this.language.equals(Language.KAZ) || this.language.equals(Language.QAZ)) {
          result.add(getFloatingPart(part));
        }

        result.add(translateFromLong(Long.parseLong(part)));

        if (this.language.equals(Language.ENG) || this.language.equals(Language.RUS)) {
          result.add(getFloatingPart(part));
        }
      }
    }

    return String.join(" ", result);
  }

  private String getFloatingPart(String part) {
    return new ThreeDigitsBlock(Integer.parseInt(part), this.language, 0, isDouble).getFloatingPartToWordsMapper(part.length());
  }

  private String[] splitByThreeDigits(long number) {
    return NumberFormat
      .getNumberInstance(Locale.US) // в Америке число разделяется запятыми, а не пробелами как у нас
      .format(number)
      .replace(",", " ")
      .split(" ");
  }

  private String[] splitDoubleToWholeAndDecimal(double number) {
    return NumberFormat
      .getNumberInstance(Locale.US)
      .format(number)
      .replace(",", "")
      .split("\\.");
  }

  private double round(double value, int places) {
    if (places < 0) {
      throw new IllegalArgumentException();
    }

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
  }

}
