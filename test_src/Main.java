import kz.greetgo.num_translator.languages.Language;
import kz.greetgo.num_translator.translator.NumTranslator;

import java.math.BigDecimal;
import java.util.Scanner;

import static kz.greetgo.num_translator.languages.Language.ENG;
import static kz.greetgo.num_translator.languages.Language.KAZ;
import static kz.greetgo.num_translator.languages.Language.QAZ;
import static kz.greetgo.num_translator.languages.Language.RUS;

public class Main {

  public static void main(String[] args) {

    System.out.println("=== STARTED PROGRAM ===");

    Scanner sc = new Scanner(System.in);

    /* ------------------------------- INTERACTIVE MODE ------------------------------- */
    //noinspection InfiniteLoopStatement
    while (true) {
      System.out.println("Input number: ");
      BigDecimal number = new BigDecimal(sc.nextLine());

      System.out.println("Input language (RUS, KAZ, ENG, QAZ): ");
      String language = sc.nextLine();

      if (language.length() != 3 || number.signum() < 0 || number.longValue() > 999_999_999L) {
        System.out.println("Invalid input.");
        continue;
      }

      Language lang = language.equalsIgnoreCase("kaz")
        ? KAZ
        : language.equalsIgnoreCase("rus")
        ? RUS
        : language.equalsIgnoreCase("eng")
        ? ENG : QAZ;

      String translate = new NumTranslator(lang).translate(number);

      System.out.println(translate);
    }
  }
}
