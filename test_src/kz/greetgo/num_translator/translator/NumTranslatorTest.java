package kz.greetgo.num_translator.translator;

import kz.greetgo.num_translator.languages.Language;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static kz.greetgo.num_translator.languages.Language.ENG;
import static kz.greetgo.num_translator.languages.Language.KAZ;
import static kz.greetgo.num_translator.languages.Language.QAZ;
import static kz.greetgo.num_translator.languages.Language.RUS;
import static org.assertj.core.api.Assertions.assertThat;

public class NumTranslatorTest {

  @SuppressWarnings("SpellCheckingInspection")
  @DataProvider
  private Object[][] translateDataProviderForLong() {
    return new Object[][]{
      {KAZ, BigDecimal.valueOf(123_123_120_100L), "бір жүз жиырма үш миллиард бір жүз жиырма үш миллион бір жүз жиырма мың бір жүз"},
      {QAZ, BigDecimal.valueOf(123_123_120_100L), "bir júz jıyrma úsh mıllıard bir júz jıyrma úsh mıllıon bir júz jıyrma myń bir júz"},
      {ENG, BigDecimal.valueOf(123_123_120_100L), "one hundred twenty three billion one hundred twenty three million one hundred twenty thousand one hundred"},
      {RUS, BigDecimal.valueOf(123_123_120_100L), "сто двадцать три миллиарда сто двадцать три миллиона сто двадцать тысяч сто"},
      {RUS, BigDecimal.valueOf(0), "ноль"},
      {KAZ, BigDecimal.valueOf(0), "нөл"},
      {QAZ, BigDecimal.valueOf(0), "nól"},
      {ENG, BigDecimal.valueOf(0), "zero"},
      {ENG, BigDecimal.valueOf(9), "nine"},
      {ENG, BigDecimal.valueOf(10), "ten"},
      {ENG, BigDecimal.valueOf(20), "twenty"},
      {ENG, BigDecimal.valueOf(19), "nineteen"},
      {ENG, BigDecimal.valueOf(12), "twelve"},
      {ENG, BigDecimal.valueOf(111), "one hundred eleven"},
      {RUS, BigDecimal.valueOf(999_999_999_999L), "девятьсот девяносто девять миллиардов девятьсот девяносто девять миллионов девятьсот девяносто девять тысяч девятьсот девяносто девять"},
      {KAZ, BigDecimal.valueOf(999_999_999_999L), "тоғыз жүз тоқсан тоғыз миллиард тоғыз жүз тоқсан тоғыз миллион тоғыз жүз тоқсан тоғыз мың тоғыз жүз тоқсан тоғыз"},
      {QAZ, BigDecimal.valueOf(999_999_999_999L), "toǵyz júz toqsan toǵyz mıllıard toǵyz júz toqsan toǵyz mıllıon toǵyz júz toqsan toǵyz myń toǵyz júz toqsan toǵyz"},
      {ENG, BigDecimal.valueOf(999_999_999_999L), "nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine"},
      {ENG, BigDecimal.valueOf(100_000_003L), "one hundred million three"},
      {RUS, BigDecimal.valueOf(111), "сто одиннадцать"},
      {RUS, BigDecimal.valueOf(1111), "одна тысяча сто одиннадцать"},
    };
  }

  @Test(dataProvider = "translateDataProviderForLong")
  public void translateFromLong(Language language, BigDecimal number, String expectedStr) {

    final NumTranslator numTranslator = new NumTranslator(language);

    //
    //
    final String translatedStr = numTranslator.translate(number);
    //
    //

    assertThat(translatedStr).isEqualTo(expectedStr);

  }

  @SuppressWarnings("SpellCheckingInspection")
  @DataProvider
  private Object[][] translateDataProviderForDouble() {
    return new Object[][]{
      {KAZ, BigDecimal.valueOf(123_123_120.111), "бір жүз жиырма үш миллион бір жүз жиырма үш мың бір жүз жиырма бүтін мыңнан бір жүз он бір"},
      {QAZ, BigDecimal.valueOf(123_123_120.111), "bir júz jıyrma úsh mıllıon bir júz jıyrma úsh myń bir júz jıyrma bútin myńnan bir júz on bir"},
      {ENG, BigDecimal.valueOf(123_123_120.111), "one hundred twenty three million one hundred twenty three thousand one hundred twenty and one hundred eleven thousandths"},
      {RUS, BigDecimal.valueOf(123_123_120.111), "сто двадцать три миллиона сто двадцать три тысячи сто двадцать целых сто одиннадцать тысячных"},
      {RUS, BigDecimal.valueOf(0.1), "ноль целых одна десятая"},
      {KAZ, BigDecimal.valueOf(0.1), "нөл бүтін оннан бір"},
      {QAZ, BigDecimal.valueOf(0.1), "nól bútin onnan bir"},
      {ENG, BigDecimal.valueOf(0.1), "one tenth"},
      {ENG, BigDecimal.valueOf(0.9), "nine tenths"},
      {ENG, BigDecimal.valueOf(10.9), "ten and nine tenths"},
      {ENG, BigDecimal.valueOf(20.9), "twenty and nine tenths"},
      {ENG, BigDecimal.valueOf(19.99), "nineteen and ninety nine hundredths"},
      {ENG, BigDecimal.valueOf(12.99), "twelve and ninety nine hundredths"},
      {ENG, BigDecimal.valueOf(111.999), "one hundred eleven and nine hundred ninety nine thousandths"},
      {RUS, BigDecimal.valueOf(999_999_999.999), "девятьсот девяносто девять миллионов девятьсот девяносто девять тысяч девятьсот девяносто девять целых девятьсот девяносто девять тысячных"},
      {KAZ, BigDecimal.valueOf(999_999_999.999), "тоғыз жүз тоқсан тоғыз миллион тоғыз жүз тоқсан тоғыз мың тоғыз жүз тоқсан тоғыз бүтін мыңнан тоғыз жүз тоқсан тоғыз"},
      {QAZ, BigDecimal.valueOf(999_999_999.999), "toǵyz júz toqsan toǵyz mıllıon toǵyz júz toqsan toǵyz myń toǵyz júz toqsan toǵyz bútin myńnan toǵyz júz toqsan toǵyz"},
      {ENG, BigDecimal.valueOf(999_999_999.999), "nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine and nine hundred ninety nine thousandths"},
      {ENG, BigDecimal.valueOf(100_000_003.103), "one hundred million three and one hundred three thousandth"},
      {ENG, BigDecimal.valueOf(111.111), "one hundred eleven and one hundred eleven thousandths"},
      {RUS, BigDecimal.valueOf(1111.11), "одна тысяча сто одиннадцать целых одиннадцать сотых"},
    };
  }

  @Test(dataProvider = "translateDataProviderForDouble")
  public void translateFromDouble(Language language, BigDecimal number, String expectedStr) {

    final NumTranslator numTranslator = new NumTranslator(language);

    //
    //
    final String translatedStr = numTranslator.translate(number);
    //
    //

    assertThat(translatedStr).isEqualTo(expectedStr);

  }
}
